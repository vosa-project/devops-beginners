#!/bin/bash

#Author: Katrin Loodus
#Modified by Oliver Tiks
#Last modify date: 10.09.2017

#Description: This script is used for copying back setup files to lab desktop and lab server after they have been modified and fetched from the repository. 

#Important! Run this script AFTER you have synced the directory with git. 

#seccopies updated run-virtualta.sh script and virtualta.desktop icon to template desktop
find /root/devops-beginners/desktop -mtime 0 -type f \( -name "run-virtualta.sh" \) -exec scp {} root@labdesktop:/home/student \;
find /root/devops-beginners/desktop -mtime 0 -type f \( -name "virtualta.desktop" \) -exec scp {} root@labdesktop:/home/student/Desktop \;

#creates symlink for autostart 
ssh root@labdesktop 'ln -fs /home/student/Desktop/virtualta.desktop /home/student/.config/autostart/virtualta.desktop'

#seccopies updated rc.local and cleanup.sh scripts to template server
find /root/devops-beginners/server -mtime 0 -type f \( -name "rc.local" -o -name "cleanup.sh" \) -exec scp {} root@labserver:/var/tmp \;
