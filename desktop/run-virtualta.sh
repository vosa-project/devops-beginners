#!/bin/sh

VTA_URL_FILE="/var/tmp/vta-url"
while [ ! -r "$VTA_URL_FILE" ]
do
        sleep 1
done
URL=$(cat "$VTA_URL_FILE")
chromium-browser --incognito --start-maximized "--app=$URL"

