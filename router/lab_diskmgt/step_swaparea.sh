#!/bin/bash
# Script for checking DEVOPS Disk Management lab objectives
# Objective name - Make swap (/dev/sdb1)

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks
#
# Date - 28.02.2017
# Version - 0.0.1

LC_ALL=C

# START
# SWAPAREA

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/swaparea"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - diskmgt server
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=swaparea


}

# User interaction: Create swap area on /dev/sdb1

SWAPAREA () {

	while true
	do

   	# Check if user has created swap area on /dev/sdb1 
    	ssh root@$IP_to_SSH 'blkid | grep /dev/sdb1 | grep TYPE=\"swap\"'  

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nSwap area on /dev/sdb1 has been created!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Swap area on /dev/sdb1 has not been created! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

SWAPAREA

exit 0

