#!/bin/bash
# Script for checking DEVOPS Disk Management lab objectives
# Objective name - Mount partitions manually

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks
#
# Date - 28.02.2017
# Version - 0.0.1

LC_ALL=C

# START
# MANUALMOUNT

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/manualmount"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - diskmgt server
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=manualmount


}

# User interaction: Mount partition /dev/sdc5 to /mnt/files and partition /dev/sdc6 to /mnt/ntfs

MANUALMOUNT () {

	while true
	do

   	# Check if user has mounted both partitions correctly
    	ssh root@$IP_to_SSH 'df -h | grep /dev/sdc5 | grep /mnt/files && df -h | grep /dev/sdc6 | grep /mnt/ntfs'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nPartitions /dev/sdc5 and /dev/sdc6 have been mounted to /mnt/files and /mnt/ntfs respectively!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Partitions /dev/sdc5 and /dev/sdc6 have not been mounted to /mnt/files and /mnt/ntfs respectively! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MANUALMOUNT

exit 0

