#!/bin/bash
# Script for checking DEVOPS Disk Management lab objectives
# Objective name - Configure mounting in /etc/fstab (/dev/sdc5 to /mnt/files)

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks
#
# Date - 28.02.2017
# Version - 0.0.1

LC_ALL=C

# START
# AUTOMOUNT1

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/automount1"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - diskmgt server
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=automount1


}

# User interaction: Automate mounting partition /dev/sdc5 to /mnt/files

AUTOMOUNT1 () {

	while true
	do

   	# Check if /dev/sdc5 has been automatically mounted to /mnt/files
    	ssh root@$IP_to_SSH 'df -h | grep /dev/sdc5 | grep /mnt/files && grep -w "$(lsblk -no UUID /dev/sdc5)\|/dev/sdc5" /etc/fstab | grep -w /mnt/files | grep -v "#" && mount -an'  

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nPartition /dev/sdc5 is automatically mounted to /mnt/files!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Partition /dev/sdc5 is not automatically mounted to /mnt/files! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

AUTOMOUNT1

exit 0

