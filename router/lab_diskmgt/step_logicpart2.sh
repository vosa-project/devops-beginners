#!/bin/bash
# Script for checking DEVOPS Disk Management lab objectives
# Objective name - Create logical partition sdc6

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks
#
# Date - 28.02.2017
# Version - 0.0.1

LC_ALL=C

# START
# LOGICPART2

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/logicpart2"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - diskmgt server
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=logicpart2


}

# User interaction: Create new logical partition /dev/sdc6

LOGICPART2 () {

	while true
	do

   	# Check if user has created logical partition /dev/sdc6 
    	ssh root@$IP_to_SSH 'lsblk | grep sdc6'  

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nDisk partition 'sdc6' has been created!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Disk partition 'sdc6' has not been created! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START
LOGICPART2

exit 0

