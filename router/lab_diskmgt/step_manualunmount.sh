#!/bin/bash
# Script for checking DEVOPS Disk Management lab objectives
# Objective name - Unmount partitions

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks
#
# Date - 28.02.2017
# Version - 0.0.1

LC_ALL=C

# START
# MANUALUNMOUNT

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/manualunmount"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - diskmgt server
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=manualunmount


}

# User interaction: Unmount partitions /dev/sdc5 and /dev/sdc6 from /mnt/files and /mnt/ntfs respectively

MANUALUNMOUNT () {

	while true
	do

   	# Check if user has unmounted /dev/sdc5 and /dev/sdc6
    	ssh root@$IP_to_SSH 'df -h | grep -ce "/mnt/files\|/mnt/ntfs" | grep -x 0'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nPartitions /dev/sdc5 and /dev/sdc6 have been unmounted!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Partition /dev/sdc5 or /dev/sdc6 has not been unmounted! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MANUALUNMOUNT

exit 0

