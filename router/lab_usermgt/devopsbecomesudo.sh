#!/bin/bash
# Script for checking DEVOPS - user management lab objectives
# Objective name - become sudo in server

# Author - Katrin Loodus
#
# Date - 31.10.2016
# Version - 0.0.1

LC_ALL=C

# START
# BECOMESUDO

# Set variables

START () {

    # Enable logging
    echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
    exec &> >(tee -a /var/log/labcheckslog.log)

    # If $CheckFile exists, then exit the script
    CheckFile="/var/labchecks/becomesudo.txt"

    if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset 	

    # Get working directory 
    DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

    # IP to SSH to - devops server
    IP_to_SSH=labserver

    # Time to sleep between running the check again
    Sleep=5

    # sshlogin.sh specific variables 

    # Objective uname in VirtualTA
    Uname=becomesudo
}

# User interaction: Become sudo in server

DEVOPSBECOMESUDO () {

    while true 
    do 

	# See if student has made him/herself sudo 
        ssh root@$IP_to_SSH cat /var/log/auth.log | grep "session opened for user root by student" 

	   # Run objectiveschecks.py and update VirtualTa with correct value 
        if [ $? -eq 0 ]; then

            echo -e "\nUse sudo to get root access is OK.  Date: `date`\n" && touch $CheckFile
            $DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectivechecks.py! Date: `date`" >&2 && exit 1
            exit 0

        else

            echo -e "Using sudo to become root is FAIL! Date: `date`\n" >&2
            sleep $Sleep

        fi
    done 

}

START

DEVOPSBECOMESUDO

exit 0

