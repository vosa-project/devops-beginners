#!/bin/bash

# description "Ordered list of linear checks for DEVOPS User Management lab"

# /root/devops-beginners/router/lab_usermgt/... || true
/root/devops-beginners/router/lab_usermgt/devopssshlogin.sh || true
/root/devops-beginners/router/lab_usermgt/devopsbecomesudo.sh || true
/root/devops-beginners/router/lab_usermgt/devopsadduser.sh || true
/root/devops-beginners/router/lab_usermgt/devopsdefshell.sh || true
/root/devops-beginners/router/lab_usermgt/devopsrmuser.sh || true
/root/devops-beginners/router/lab_usermgt/devopslockuser.sh || true
/root/devops-beginners/router/lab_usermgt/devopsunlockuser.sh || true
/root/devops-beginners/router/lab_usermgt/devopsaddgrp.sh || true
/root/devops-beginners/router/lab_usermgt/devopsmodgrp.sh || true
/root/devops-beginners/router/lab_usermgt/devopsrmusergrp.sh || true
/root/devops-beginners/router/lab_usermgt/devopsrmgrp.sh || true

