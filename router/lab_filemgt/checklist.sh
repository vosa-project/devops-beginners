#!/bin/bash

# description "Ordered list of linear checks for DEVOPS File Management lab"

# /root/devops-beginners/router/lab_filemgt/... || true
/root/devops-beginners/router/lab_filemgt/devopssshlogin2.sh || true
/root/devops-beginners/router/lab_filemgt/devopsbecomesudo2.sh || true
/root/devops-beginners/router/lab_filemgt/devopsuploaddir.sh || true
/root/devops-beginners/router/lab_filemgt/devopsfindupload.sh || true
/root/devops-beginners/router/lab_filemgt/devopsbackupfolder.sh || true
/root/devops-beginners/router/lab_filemgt/devopsbuscript.sh || true
/root/devops-beginners/router/lab_filemgt/devopsbuscriptperm.sh || true
/root/devops-beginners/router/lab_filemgt/devopssetgid.sh || true
/root/devops-beginners/router/lab_filemgt/devopstestsetgid.sh || true
/root/devops-beginners/router/lab_filemgt/devopschown.sh || true
/root/devops-beginners/router/lab_filemgt/devopschgrp.sh || true

