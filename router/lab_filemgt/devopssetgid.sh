#!/bin/bash
# Script for checking DEVOPS - file and folder management lab objectives
# Objective name - creating uploads folder

# Author - Katrin Loodus
#
# Date - 28.03.2017 
# Version - 0.0.1

LC_ALL=C

# START
# UPLOADDIR

# Set variables

START () {

    # Enable logging
    echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
    exec &> >(tee -a /var/log/labcheckslog.log)

    # If $CheckFile exists, then exit the script
    CheckFile="/var/labchecks/setgid.txt"

    if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset 	

    # Get working directory 
    DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

    # IP to SSH to - devops server
    IP_to_SSH=labserver

    # Time to sleep between running the check again
    Sleep=5

    # uploaddir.sh specific variables 

    # Objective uname in VirtualTA
    Uname=setgid

    # User connecting via SSH
    SSH_User=student
}

# User interaction: Log into the server via ssh

DEVOPSSETGID () {

    while true 
    do 

	   # See if student has logged into the server
           ssh root@$IP_to_SSH ls -ld /uploads |grep uploaders |grep sr

	   # Run objectiveschecks.py and update VirtualTa with correct value 
        if [ $? -eq 0 ]; then

            echo -e "\nFolder /uploads is present and has a setgid permission.  Date: `date`\n" && touch $CheckFile
            $DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectivechecks.py! Date: `date`" >&2 && exit 1
            exit 0

        else

            echo -e "Folder /uploads is not present or it doesn't have proper user permissions! Date: `date`\n" >&2
            sleep $Sleep

        fi
    done 

}

START

DEVOPSSETGID

exit 0

