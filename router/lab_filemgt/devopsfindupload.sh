#!/bin/bash
# Script for checking DEVOPS - file and folder management lab objectives
# Objective name - creating uploads folder

# Author - Katrin Loodus
#
# Date - 28.03.2017 
# Version - 0.0.1

LC_ALL=C

# START
# FINDUPLOAD

# Set variables

START () {

    # Enable logging
    echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
    exec &> >(tee -a /var/log/labcheckslog.log)

    # If $CheckFile exists, then exit the script
    CheckFile="/var/labchecks/findupload.txt"

    if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset 	

    # Get working directory 
    DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

    # IP to SSH to - devops server
    IP_to_SSH=labserver

    # Time to sleep between running the check again
    Sleep=5

    # uploaddir.sh specific variables 

    # Objective uname in VirtualTA
    Uname=findupload

    # User connecting via SSH
    SSH_User=student
}

# User interaction: Log into the server via ssh

DEVOPSFINDUPLOAD () {

    while true 
    do 

	   # See if student has logged into the server
           ssh root@$IP_to_SSH ls -l /uploads  |grep angel

	   # Run objectiveschecks.py and update VirtualTa with correct value 
        if [ $? -eq 0 ]; then

            echo -e "\nAngels files in folder /uploads have been created.  Date: `date`\n" && touch $CheckFile
            $DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectivechecks.py! Date: `date`" >&2 && exit 1
            exit 0

        else

            echo -e "Angels files in folder /uploads not present! Date: `date`\n" >&2
            sleep $Sleep

        fi
    done 

}

START

DEVOPSFINDUPLOAD

exit 0

