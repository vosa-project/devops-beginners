#!/bin/bash

export LAB_USERNAME=$(dmidecode -s bios-release-date)

# Download lab

counter=10

while [ $counter -gt 0  ]; do

curl -H "accept: application/json" -X POST "$(dmidecode -s system-product-name)/labinfo?uuid=$(dmidecode -s system-version)" > /tmp/lab_data

        if [ $? -eq 0 ]; then
                break
        fi
        sleep 2
        $((counter--))
done


export LAB_NAME=$(cat /tmp/lab_data | jq -r '.lab.name')
export LAB_USERNAME=$(cat /tmp/lab_data | jq -r '.user.username')
export TA_KEY=$(cat /tmp/lab_data | jq -r '.lab.lab_token')
export VIRTUALTA_HOSTNAME=$(cat /tmp/lab_data | jq -r '.assistant.uri')
export USER_FULLNAME=$(cat /tmp/lab_data | jq -r '.user.name')
export USER_KEY=$(cat /tmp/lab_data | jq -r '.user.user_key')
export LAB_ID=$(cat /tmp/lab_data | jq -r '.lab.lab_hash')

# Create location for permanent checkfiles
DIR="/var/labchecks"
[ ! -d "$DIR" ] && mkdir "$DIR"

# Create lab.ini file for updating vTA 

cat > /root/devops-beginners/lab.ini <<EOC

[LAB]
ta_key = $TA_KEY
virtualta_hostname = $VIRTUALTA_HOSTNAME
lab_id = $LAB_ID
EOC

# Send vTA url to Desktop

counter=10

while [ $counter -gt 0  ]; do

        ssh root@labdesktop "echo \"$VIRTUALTA_HOSTNAME\"/lab/\"$LAB_ID\"/\"$USER_KEY\" > /var/tmp/vta-url"

        if [ $? -eq 0 ]; then
                break
        fi
        sleep 2
        $((counter--))
done



# Select lab checklist. Add path for each lab that uses the same template set

# DEVOPS User Management
dmidecode -s bios-version |grep 'UserMgt-' >/dev/null 2>&1
if [ $? -eq 0 ]
then
	PATH=/root/devops-beginners/router/lab_usermgt:"$PATH"
fi

#DEVOPS File Management
dmidecode -s bios-version |grep 'FileMgt-' >/dev/null 2>&1
if [ $? -eq 0 ]
then
	PATH=/root/devops-beginners/router/lab_filemgt:"$PATH"
fi

#DEVOPS Disk Management
dmidecode -s bios-version |grep 'DiskMgt-' >/dev/null 2>&1
if [ $? -eq 0 ]
then
	PATH=/root/devops-beginners/router/lab_diskmgt:"$PATH"
fi

